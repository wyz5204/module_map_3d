package com.atom.map.util.array

class SimpleFloatArray {
    companion object {
        protected val MIN_CAPACITY_INCREMENT = 12
        protected val EMPTY_ARRAY = FloatArray(0)
    }

    protected lateinit var array: FloatArray

    var size = 0

    var capacity = 0;

    constructor(initialCapacity: Int = 0) {
        init(initialCapacity)
    }

    private fun init(initialCapacity: Int = 0) {
        array = if (initialCapacity == 0) {
            EMPTY_ARRAY
        } else {
            FloatArray(initialCapacity)
        }
        capacity = initialCapacity;
    }

    fun array(): FloatArray {
        return array
    }

    fun size(): Int {
        return size
    }

    operator fun get(index: Int): Float {
        return array[index]
    }

    operator fun set(index: Int, value: Float, isAdd: Boolean): SimpleFloatArray {
        array[index] = value
        if (isAdd) size++
        return this
    }

    fun add(value: Float): SimpleFloatArray {
        val capacity = array.size
        if (capacity == size) {
            val increment = Math.max(capacity shr 1, MIN_CAPACITY_INCREMENT)
            val newArray = FloatArray(capacity + increment)
            System.arraycopy(array, 0, newArray, 0, capacity)
            array = newArray
        }
        array[size++] = value
        return this
    }

    fun trimToSize(): SimpleFloatArray {
        val size = size
        if (size == array.size) {
            return this
        }
        if (size == 0) {
            array = EMPTY_ARRAY
        } else {
            val newArray = FloatArray(size)
            System.arraycopy(array, 0, newArray, 0, size)
            array = newArray
        }
        return this
    }

    fun clear(): SimpleFloatArray {
        array = EMPTY_ARRAY
        size = 0
        return this
    }

    fun isEmpty() = size == 0

    fun resize(i: Int) {
        init(i)
    }
}