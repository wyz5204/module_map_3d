package com.atom.map.navigator

import com.atom.map.WorldWindow

interface NavigatorListener {

    fun onNavigatorEvent(wwd: WorldWindow, event: NavigatorEvent)

}