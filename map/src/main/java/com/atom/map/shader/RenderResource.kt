package com.atom.map.shader

import com.atom.map.drawable.DrawContext

interface RenderResource {

    fun release(dc : DrawContext)

}
