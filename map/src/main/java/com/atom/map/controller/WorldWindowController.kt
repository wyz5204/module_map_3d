package com.atom.map.controller

import android.view.MotionEvent
import com.atom.map.WorldWindow

interface WorldWindowController {

    var world : WorldWindow?

    fun onTouchEvent(event: MotionEvent): Boolean
}