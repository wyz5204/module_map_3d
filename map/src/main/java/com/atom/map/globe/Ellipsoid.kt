package com.atom.map.globe

class Ellipsoid {
    /**
     * 椭球长半径一半
     */
    private val semiMajorAxis : Double
    private val semiMajorAxisDouble : Double
    /**
     * 椭球短半径一半
     */
    private val semiMinorAxis : Double
    private val semiMinorAxisDouble : Double

    /**
     * 椭球逆扁率
     */
    private val inverseFlattening : Double

    /**
     * 椭球偏心率
     */
    private val eccentricitySquared : Double

    constructor(ellipsoid: Ellipsoid) : this(ellipsoid.semiMajorAxis , ellipsoid.inverseFlattening)

    constructor(
        semiMajorAxis: Double,
        inverseFlattening: Double
    ) {
        val f = 1 / inverseFlattening
        this.inverseFlattening = inverseFlattening
        this.semiMajorAxis = semiMajorAxis
        this.semiMajorAxisDouble = semiMajorAxis * semiMajorAxis
        this.semiMinorAxis = semiMajorAxis * (1 - f)
        this.semiMinorAxisDouble = semiMinorAxis * semiMinorAxis
        this.eccentricitySquared = 2 * f - f * f
    }

    override fun equals(other: Any?): Boolean {
        if (other == null || this.javaClass != other.javaClass) {
            return false
        }
        val that = other as Ellipsoid
        return (semiMajorAxis == that.semiMajorAxis
                && inverseFlattening == that.inverseFlattening)
    }
    override fun hashCode(): Int {
        var result: Int
        var temp: Long = semiMajorAxis.toBits()
        result = (temp xor (temp ushr 32)).toInt()
        temp = inverseFlattening.toBits()
        result = 31 * result + (temp xor (temp ushr 32)).toInt()
        return result
    }

    fun semiMajorAxis(): Double = semiMajorAxis
    fun semiMajorAxisDouble(): Double = semiMajorAxisDouble
    fun semiMinorAxis(): Double = semiMinorAxis
    fun semiMinorAxisDouble(): Double = semiMinorAxisDouble
    fun inverseFlattening(): Double  = inverseFlattening
    fun eccentricitySquared(): Double  = eccentricitySquared

}